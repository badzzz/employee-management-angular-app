import { IEmployee } from './IEmployee';

export const EMPLOYEES: IEmployee[] = [
  {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    dateOfBirth: '1980-01-01',
    position: 'Production worker',
    salary: 2900,
  },
  {
    id: 2,
    firstName: 'Peter',
    lastName: 'Baz',
    dateOfBirth: '1990-01-01',
    position: 'Production worker',
    salary: 2600,
  },
  {
    id: 3,
    firstName: 'Jane',
    lastName: 'Smith',
    dateOfBirth: '1987-01-01',
    position: 'Production worker',
    salary: 3300,
  },
  {
    id: 4,
    firstName: 'Rachel',
    lastName: 'Adams',
    dateOfBirth: '1988-01-01',
    position: 'Production worker',
    salary: 3100,
  },
  {
    id: 5,
    firstName: 'Richard',
    lastName: 'Howard',
    dateOfBirth: '1977-01-01',
    position: 'Manager',
    salary: 5300,
  },
  {
    id: 6,
    firstName: 'Mover',
    lastName: 'Benard',
    dateOfBirth: '1923-04-03',
    position: 'Tester',
    salary: 6200,
  },
];
